#include <iostream>
#include <utility>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define WIN_NAME "Filtro de sentido"
#define RASTRO_L 64

typedef struct {
		cv::Mat buffer;
		cv::Point2d a, b, ponto, ponto_anterior;
		cv::Point2d rastro[RASTRO_L];
		cv::Scalar degrade[RASTRO_L], cor_1, cor_2, cor;
		unsigned char rastro_i;
		bool sentido;
} Dados;

void Construtor(Dados *dados);
void CallBack_Mouse(int event, int x, int y, int flags, void* userdata);
void DesenhaDados(Dados *dados);
int Cross(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b);	//CCW: +, CW: -, Collinear: 0. Fazer alias CW, CCW e COLINEAR
bool CCW(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b);
bool CW(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b);
bool Collinear(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b);
int Cruza(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d);
int CruzaOrientado(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d);

int main(int, char **) {
	Dados dados;

	Construtor(&dados);
	DesenhaDados(&dados);
	cv::imshow(WIN_NAME, dados.buffer);
	cv::waitKey();
	return 0;
}

void Construtor(Dados *dados) {
	dados->a = cv::Point2d(400, 200);
	dados->b = dados->ponto = dados->ponto_anterior = cv::Point2d(200, 200);
	dados->buffer = cv::Mat(600, 800, CV_8UC3);
	cv::namedWindow(WIN_NAME, cv::WINDOW_AUTOSIZE | CV_GUI_NORMAL);
	cv::setMouseCallback(WIN_NAME, CallBack_Mouse, dados);
	dados->rastro_i = 0;
	int r = 256/RASTRO_L;
	for(int i = 0; i < RASTRO_L; i++) {
		dados->degrade[i] = cv::Scalar(i*r/4, i*r + 1, i*r + 1);
		dados->rastro[i] = dados->ponto;
	}
	dados->cor = dados->cor_1 = cv::Scalar(192, 96, 128);
	dados->cor_2 = cv::Scalar(128, 192, 96);
	dados->sentido = true;
}

void CallBack_Mouse(int event, int x, int y, int flags, void* userdata)	{
	Dados *dados = (Dados *)userdata;
	switch(event)	{
		case cv::EVENT_LBUTTONDOWN:
			dados->a = dados->b;
			dados->b = dados->ponto;
			break;
		case cv::EVENT_RBUTTONDOWN:
			break;
		case cv::EVENT_RBUTTONUP:
			dados->sentido = ! dados->sentido;
			break;
		case cv::EVENT_LBUTTONUP:
			break;
		case cv::EVENT_MOUSEMOVE:
			dados->ponto_anterior = dados->ponto;
			dados->ponto = cv::Point2d(x, y);
			dados->rastro[dados->rastro_i] = dados->ponto;
			dados->rastro_i = (dados->rastro_i +1)%RASTRO_L;
			if(dados->sentido && CruzaOrientado(dados->a, dados->b, dados->ponto_anterior, dados->ponto)) {
				dados->cor = dados->cor_2;
				dados->cor_2 = dados->cor_1;
				dados->cor_1 = dados->cor;
			}
			else if(!dados->sentido && Cruza(dados->a, dados->b, dados->ponto_anterior, dados->ponto)) {
				dados->cor = dados->cor_2;
				dados->cor_2 = dados->cor_1;
				dados->cor_1 = dados->cor;
			}
	}
	DesenhaDados(dados);
}

int Cross(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b) {
	return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

bool CCW(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b) {
	if(Cross(o, a, b) > 0) {
		return true;
	}
	return false;
}

bool CW(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b) {
	if(Cross(o, a, b) < 0) {
		return true;
	}
	return false;
}

bool Colinear(const cv::Point2d &o, const cv::Point2d &a, const cv::Point2d &b) {
	if(Cross(o, a, b) == 0) {
		return true;
	}
	return false;
}

int Cruza(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d) {
	return (CCW(a, c, d) != CCW(b, c, d)) && (CCW(a, b, c) != CCW(a, b, d));
}

int CruzaOrientado(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d) {
	return CCW(a, c, d)  && (CCW(a, c, d) != CCW(b, c, d)) && (CCW(a, b, c) != CCW(a, b, d));
}

void DesenhaDados(Dados *dados) {
	std::stringstream notificacao_ss;
	notificacao_ss << "<" << dados->ponto.x << ", " << dados->ponto.y << ">";
	dados->buffer = cv::Scalar(0);
	cv::putText(dados->buffer, "Clique na tela para alterar as extremidades da flecha.", cv::Point(15,20), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(66, 167, 244), 1, cv::LINE_8, false);
	cv::putText(dados->buffer, "Clique com o botao direito para ligar ou desligar o filtro de sentido.", cv::Point(15,40), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(66, 167, 244), 1, cv::LINE_8, false);
	if(dados->sentido == false) {
		cv::putText(dados->buffer, "Filtro de sentido DESATIVADO.", cv::Point(15,60), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 0, 255), 1, cv::LINE_8, false);
		cv::putText(dados->buffer, "A flecha muda de cor quando cruzada pelo o rastro do mouse", cv::Point(15,80), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(66, 167, 244), 1, cv::LINE_8, false);
	}
	else {
		cv::putText(dados->buffer, "Filtro de sentido ATIVADO.", cv::Point(15,60), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 0, 255), 1, cv::LINE_8, false);
		cv::putText(dados->buffer, "A flecha muda de cor SOMENTE QUANDO o rastro do mouse cruza ela PELO LADO ESQUERDO", cv::Point(15,80), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(66, 167, 244), 1, cv::LINE_8, false);
	}
	cv::putText(dados->buffer, "Mouse: " + notificacao_ss.str(), cv::Point(15,580), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 255, 0), 1, cv::LINE_8, false);
	cv::arrowedLine(dados->buffer, dados->a, dados->b, dados->cor);
	cv::rectangle(dados->buffer, dados->a - cv::Point2d(2, 2), dados->a + cv::Point2d(3, 3), cv::Scalar(0, 192, 96), -1, cv::LINE_8, 0);
	cv::rectangle(dados->buffer, dados->b - cv::Point2d(2, 2), dados->b + cv::Point2d(3, 3), cv::Scalar(0, 192, 96), -1, cv::LINE_8, 0);
	for(int i = 0; i < RASTRO_L - 1; i++) {
		cv::line(dados->buffer, dados->rastro[(i + dados->rastro_i)%RASTRO_L], dados->rastro[(i + dados->rastro_i + 1)%RASTRO_L], dados->degrade[i+1]);
	}
	cv::imshow(WIN_NAME, dados->buffer);
//	cv::updateWindow("Point-Line Projection");	//NOTE: No OpenGL overlay, so not needed.
}
